package com.secondstack.training.spring.extjs.domain;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Adrian
 * Date: 10/01/13
 * Time: 14:39
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="document")
public class Document{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "document_id")
    private Integer id;

    @Column(name="original_name")
    private String originalName;

    @Column(name="content_type", nullable = false)
    private String contentType;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name="content", nullable = false)
    private byte[] content;

    @Column(name="info")
    private String info;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
