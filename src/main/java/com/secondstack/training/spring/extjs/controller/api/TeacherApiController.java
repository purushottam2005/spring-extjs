package com.secondstack.training.spring.extjs.controller.api;

import com.secondstack.training.spring.extjs.domain.Teacher;
import com.secondstack.training.spring.extjs.repository.TeacherRepository;
import com.secondstack.training.spring.extjs.service.TeacherService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
//@RequestMapping(value = "teacher", headers = {"Accept=application/json"})
@RequestMapping(value = "/api/teacher")
public class TeacherApiController {

    protected static Logger logger = Logger.getLogger("controller");

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private TeacherRepository teacherRepository;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody Teacher teacher){
        logger.debug("Received rest request to create teacher");
//        teacherService.save(teacher);
        teacherRepository.save(teacher);
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestParam("id")Integer id, @RequestBody Teacher teacher){
        logger.debug("Received rest request to update teacher");
//        teacherService.update(id, teacher);

        Teacher oldTeacher = teacherRepository.findOne(id);
        oldTeacher.setAcademicDegree(teacher.getAcademicDegree());
        oldTeacher.setAddress(teacher.getAddress());
        oldTeacher.setFirstName(teacher.getFirstName());
        oldTeacher.setLastName(teacher.getLastName());
        oldTeacher.setSex(teacher.getSex());
        teacherRepository.save(oldTeacher);
    }

//    @RequestMapping(method = RequestMethod.GET)
//    @ResponseBody
//    public List<Teacher> findAll(){
//        logger.debug("Received rest request to get list teacher");
////        List<Teacher> teacherList = teacherService.findAll();
//        List<Teacher> teacherList = teacherRepository.findAll();
//        return teacherList;
//    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Page<Teacher> findAll(Pageable pageable){
        logger.debug("Received rest request to get list teacher");
        Page<Teacher> teacherPage = teacherRepository.findAll(pageable);
        return teacherPage;
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.GET)
    @ResponseBody
    public Teacher findById(@RequestParam("id")Integer id){
        logger.debug("Received rest request to get data teacher");
//        Teacher teacher = teacherService.findById(id);
        Teacher teacher = teacherRepository.findOne(id);
        return teacher;
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@RequestParam("id")Integer id){
        logger.debug("Received rest request to delete teacher");
//        teacherService.delete(id);
        teacherRepository.delete(id);
    }

}
