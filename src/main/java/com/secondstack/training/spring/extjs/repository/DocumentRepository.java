package com.secondstack.training.spring.extjs.repository;

import com.secondstack.training.spring.extjs.domain.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/6/13
 * Time: 10:55 PM
 * To change this template use File | Settings | File Templates.
 */
public interface DocumentRepository extends JpaRepository<Document, Integer> {

    @Query("select o from Document o where o.originalName like :originalName")
    Page<Document> findByOriginalName(@Param("originalName")String originalName, Pageable pageable);
}
