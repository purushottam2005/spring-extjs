package com.secondstack.training.spring.extjs.service.impl;

import com.secondstack.training.spring.extjs.domain.Lecture;
import com.secondstack.training.spring.extjs.domain.Subject;
import com.secondstack.training.spring.extjs.domain.Teacher;
import com.secondstack.training.spring.extjs.domain.enumeration.Semester;
import com.secondstack.training.spring.extjs.service.LectureService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:26 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
public class LectureServiceImpl implements LectureService {

    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public void save(Lecture lecture) {
        entityManager.persist(lecture);
    }

    @Override
    public Lecture update(Integer id, Lecture lecture) {
        Lecture oldLecture = findById(id);
        oldLecture.setSemester(lecture.getSemester());
        oldLecture.setSubject(lecture.getSubject());
        oldLecture.setTeacher(lecture.getTeacher());
        oldLecture.setYear(lecture.getYear());
        return entityManager.merge(oldLecture);
    }

    @Override
    public void delete(Lecture lecture) {
        entityManager.remove(lecture);
    }

    @Override
    public void delete(Integer id) {
        entityManager.remove(findById(id));
    }

    @Override
    public List<Lecture> findAll() {
        List<Lecture> results = entityManager.createQuery("SELECT o from Lecture o", Lecture.class).getResultList();
        return results;
    }

    @Override
    public Lecture findById(Integer id) {
        return entityManager.find(Lecture.class, id);
    }

    public List<Lecture> findByTeacher(Teacher teacher) {
        return entityManager.createQuery(
                "select o from Lecture o where o.teacher = :teacher"
                , Lecture.class).setParameter("teacher", teacher).getResultList();
    }

    public List findSimpleByTeacher1(Teacher teacher) {
        return entityManager.createQuery(
                "select o.id, o.semester, o.year, o.subject.name, o.teacher.firstName from Lecture o where o.teacher = :teacher")
                .setParameter("teacher", teacher).getResultList();
    }

    public List findSimpleByTeacher2(Teacher teacher) {
        return entityManager.createQuery(
                "select new map(o.id, o.semester as semester, o.year as year, o.subject.name as subjectName, " +
                        "o.teacher.firstName as teacherFirstName) from Lecture o where o.teacher = :teacher")
                .setParameter("teacher", teacher).getResultList();
    }

    public List findSimpleByTeacher3(Teacher teacher) {
        return entityManager.createQuery(
                "select new com.secondstack.training.spring.extjs.service.dto.SimpleLecture" +
                        "(o.id, o.semester, o.year, o.subject.name, o.teacher.firstName) " +
                        "from Lecture o where o.teacher = :teacher")
                .setParameter("teacher", teacher).getResultList();
    }

    @Override
    public Lecture findBySubjectAndTeacherAndSemesterAndYear(Subject subject, Teacher teacher, Semester semester, Integer year) {
        return entityManager.createQuery(
                "SELECT o FROM Lecture o " +
                        "where o.subject = :subject AND o.teacher = :teacher AND o.semester = :semester AND o.year = :year"
                , Lecture.class)
                .setParameter("subject", subject).setParameter("teacher", teacher)
                .setParameter("semester", semester).setParameter("year", year)
                .getSingleResult();
    }
}
