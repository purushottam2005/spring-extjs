package com.secondstack.training.spring.extjs.mixin;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/6/13
 * Time: 11:16 PM
 * To change this template use File | Settings | File Templates.
 */
@JsonIgnoreProperties({"content"})
public interface DocumentMixin {
}
