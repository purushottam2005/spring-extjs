package com.secondstack.training.spring.extjs.domain;

import com.secondstack.training.spring.extjs.domain.enumeration.Semester;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/25/13
 * Time: 10:00 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "lecture")
public class Lecture {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "lecture_id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "subject_id", nullable = false)
    private Subject subject;

    @ManyToOne
    @JoinColumn(name = "teacher_id", nullable = false)
    private Teacher teacher;

    @Column(name = "semester", nullable = false)
    private Semester semester;

    @Column(name = "year", nullable = false)
    private Integer year;
//
//    @OneToMany(fetch = FetchType.LAZY)
//    @JoinColumn(name = "lecture_id", nullable = false)
//    private Set<Participant> participantSet;

    public Lecture() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

//    public Set<Participant> getParticipantSet() {
//        return participantSet;
//    }
//
//    public void setParticipantSet(Set<Participant> participantSet) {
//        this.participantSet = participantSet;
//    }
}
