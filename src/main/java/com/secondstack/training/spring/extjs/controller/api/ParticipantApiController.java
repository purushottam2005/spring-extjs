package com.secondstack.training.spring.extjs.controller.api;

import com.secondstack.training.spring.extjs.domain.Participant;
import com.secondstack.training.spring.extjs.service.ParticipantService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "participant", headers = {"Accept=application/json"})
//@RequestMapping(value = "/api/participant")
public class ParticipantApiController {

    protected static Logger logger = Logger.getLogger("controller");

    @Autowired
    private ParticipantService participantService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody Participant participant){
        logger.debug("Received rest request to create participant");
        participantService.save(participant);
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestParam("id")Integer id, @RequestBody Participant participant){
        logger.debug("Received rest request to update participant");
        participantService.update(id, participant);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<Participant> findAll(ModelMap modelMap){
        logger.debug("Received rest request to get list participant");
        List<Participant> participantList = participantService.findAll();
        return participantList;
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.GET)
    @ResponseBody
    public Participant findById(@RequestParam("id")Integer id){
        logger.debug("Received rest request to get data participant");
        Participant participant = participantService.findById(id);
        return participant;
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@RequestParam("id")Integer id){
        logger.debug("Received rest request to delete participant");
        participantService.delete(id);
    }

}
