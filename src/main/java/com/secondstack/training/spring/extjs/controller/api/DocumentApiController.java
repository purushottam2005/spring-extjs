package com.secondstack.training.spring.extjs.controller.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.secondstack.training.spring.extjs.mixin.DocumentMixin;
import com.secondstack.training.spring.extjs.domain.Document;
import com.secondstack.training.spring.extjs.repository.DocumentRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/api/document")
public class DocumentApiController {
    protected static Logger logger = Logger.getLogger("controller");

    @Autowired
    private DocumentRepository documentRepository;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Map upload(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request) throws IOException {
        logger.debug("Received rest request to upload document");
        Document document = new Document();
        document.setContent(file.getBytes());
        document.setContentType(file.getContentType());
        document.setOriginalName(file.getOriginalFilename());
        document.setInfo(request.getParameter("info"));

        documentRepository.save(document);

        Map<String, Object> result = new HashMap<String, Object>();
        result.put("success", true);
        return result;
    }

    @RequestMapping(value = "/download", params = {"id"}, method = RequestMethod.GET)
    public void download(@RequestParam("id") Integer id, HttpServletResponse response) throws IOException {
        Document document = documentRepository.findOne(id);

        response.setHeader("Content-Disposition", "inline;filename=\"" + document.getOriginalName() + "\"");
        response.setContentType(document.getContentType());

        OutputStream out = response.getOutputStream();
        out.write(document.getContent());
        out.flush();
        out.close();
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public String findAll(Pageable pageable) throws IOException {
        logger.debug("Received rest request to get list document");
        Page<Document> documentPage = documentRepository.findAll(pageable);

        Map mixins = new HashMap();
        mixins.put(Document.class, DocumentMixin.class);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setMixInAnnotations(mixins);
        return objectMapper.writeValueAsString(documentPage);
    }

    @RequestMapping(params = {"originalName"},method = RequestMethod.GET)
    @ResponseBody
    public String findByOriginalName(@RequestParam("originalName")String originalName, Pageable pageable) throws IOException {
        logger.debug("Received rest request to get list document");
        Page<Document> documentPage = documentRepository.findByOriginalName(originalName, pageable);

        Map mixins = new HashMap();
        mixins.put(Document.class, DocumentMixin.class);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setMixInAnnotations(mixins);
        return objectMapper.writeValueAsString(documentPage);
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.GET)
    @ResponseBody
    public String findById(@RequestParam("id")Integer id) throws IOException {
        logger.debug("Received rest request to get data document");
        Document document = documentRepository.findOne(id);

        Map mixins = new HashMap();
        mixins.put(Document.class, DocumentMixin.class);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setMixInAnnotations(mixins);
        return objectMapper.writeValueAsString(document);
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@RequestParam("id")Integer id){
        logger.debug("Received rest request to delete document");
        documentRepository.delete(id);
    }

}
