package com.secondstack.training.spring.extjs.repository;

import com.secondstack.training.spring.extjs.domain.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/6/13
 * Time: 10:02 PM
 * To change this template use File | Settings | File Templates.
 */
public interface TeacherRepository extends JpaRepository<Teacher, Integer> {
}
