package com.secondstack.training.spring.extjs.service;

import com.secondstack.training.spring.extjs.domain.Teacher;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:25 AM
 * To change this template use File | Settings | File Templates.
 */
public interface TeacherService {
    void save(Teacher teacher);
    Teacher update(Integer id, Teacher teacher);
    void delete(Teacher teacher);
    void delete(Integer id);
    List<Teacher> findAll();
    Teacher findById(Integer id);
}
