package com.secondstack.training.spring.extjs.controller.api;

import com.secondstack.training.spring.extjs.domain.Lecture;
import com.secondstack.training.spring.extjs.repository.LectureRepository;
import com.secondstack.training.spring.extjs.service.LectureService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/api/lecture")
public class LectureApiController {

    protected static Logger logger = Logger.getLogger("controller");

    @Autowired
    private LectureService lectureService;

    @Autowired
    private LectureRepository lectureRepository;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody Lecture lecture){
        logger.debug("Received rest request to create lecture");
        lectureService.save(lecture);
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestParam("id")Integer id, @RequestBody Lecture lecture){
        logger.debug("Received rest request to update lecture");
        lectureService.update(id, lecture);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Page<Lecture> findAll(Pageable pageable){
        logger.debug("Received rest request to get list lecture");
        Page<Lecture> lectures = lectureRepository.findAll(pageable);
        return lectures;
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.GET)
    @ResponseBody
    public Lecture findById(@RequestParam("id")Integer id){
        logger.debug("Received rest request to get data lecture");
        Lecture lecture = lectureRepository.findOne(id);
        return lecture;
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@RequestParam("id")Integer id){
        logger.debug("Received rest request to delete lecture");
        lectureService.delete(id);
    }

}
