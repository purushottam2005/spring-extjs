/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/3/13
 * Time: 10:55 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('SpringExtJS.store.LectureStore', {
    extend:'Ext.data.Store',
    model:'SpringExtJS.model.Lecture',
    autoLoad:true,
    pageSize:50,

    constructor:function (cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([
            Ext.apply({
                proxy:{
                    type:'ajax',
                    pageParam:'page.page',
                    url:lectureApiUrl,
                    extraParams:function () {
                        if (me.pageSize) {
                            return {
                                'page.size':me.pageSize
                            };
                        }
                    }(),
                    reader:{
                        type:'json',
                        root:'content',
                        totalProperty:'totalElements'
                    }
                }
            }, cfg)
        ]);
    }
});