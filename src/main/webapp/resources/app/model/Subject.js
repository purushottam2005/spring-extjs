/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/3/13
 * Time: 10:52 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('SpringExtJS.model.Subject', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name:'id',
            type:'int'
        },
        {
            name:'code',
            type:'string'
        },
        {
            name:'name',
            type:'string'
        },
        {
            name:'sks',
            type:'int'
        }
    ]
});