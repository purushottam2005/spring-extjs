/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/2/13
 * Time: 9:43 PM
 * To change this template use File | Settings | File Templates.
 */
Ext.application({
    name:'SpringExtJS',
    appFolder:'resources/app',
    controllers:[
        'AppController', 'StudentController', 'TeacherController', 'LectureController', 'DocumentController'
    ],
    views:[],
    launch:function () {
        Ext.create('Ext.container.Viewport', {
            layout:'fit',
            items:[
                {
                    xtype:'panel',
                    title:'2ndStack Java Training - Spring + ExtJs ',
                    layout:{
                        type:'vbox',
                        align:'stretch'
                    },
                    items:[
                        {
                            xtype:'tabpanel',
                            flex:1,
                            items:[]
                        }
                    ],
                    tbar: [
                        {
                            xtype: 'button',
                            text: 'Student',
                            action:'student'
                        },
                        {
                            xtype: 'button',
                            text: 'Teacher',
                            action:'teacher'
                        },
                        {
                            xtype: 'button',
                            text: 'Lecture',
                            action:'lecture'
                        },
                        {
                            xtype: 'button',
                            text: 'Document',
                            action:'document'
                        }
                    ]
                }
            ]
        });
    }
});