/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/3/13
 * Time: 10:46 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('SpringExtJS.controller.StudentController', {
    extend:'Ext.app.Controller',

    views:[
        'student.StudentGridList',
        'student.StudentEditWindow'
    ],

    stores:[
    ],

    /**
     * query selector dari component Ext JS.
     */
    refs:[
        {
            ref:'studentGridList',
            selector:'studentGridList'
        },
        {
            ref:'studentGridListDeleteButton',
            selector:'studentGridList button[action=delete]'
        },
        {
            ref:'studentEditWindow',
            selector:'studentEditWindow'
        },
        {
            ref:'studentEditWindowForm',
            selector:'studentEditWindow form'
        }
    ],

    /**
     * init..disini mendefinisikan even untuk component yang ada.
     */
    init:function () {
        this.control({
            'studentGridList':{ //selector component, dibawahnya adalah even even yang anda definisikan
                selectionchange:this.studentGridListSelectionChange,
                itemdblclick:this.studentGridListItemDblClick
            },
            'studentGridList button[action=add]':{
                click:this.buttonAddStudentClick
            },
            'studentGridList button[action=delete]':{
                click:this.buttonDeleteStudentClick
            },
            'studentEditWindow button[action=save]':{
                click:this.studentEditWindowButtonSaveClick
            }
        })
    },

    /**
     * Even ketika tombol Delete Student ditekan. Yaitu menghapus data pada StudentGridList yang dipilih.
     */
    buttonDeleteStudentClick:function () {
        var me = this;
        Ext.Msg.show({
            title:'Are u sure?',
            msg:'Are u sure?',
            width:300,
            buttons:Ext.Msg.YESNO,
            icon:Ext.window.MessageBox.INFO,
            fn:function (buttonId) {
                if (buttonId == 'yes') {
                    me.deleteStudent();
                }
            }
        });
    },

    deleteStudent:function () {
        var me = this;
        var record = this.getStudentGridList().getSelectionModel().getSelection()[0];
        if (record) {
            Ext.Ajax.request({
                url:studentApiUrl + "?id=" + record.data.id,
                method:'DELETE',
                headers:{
                    'Content-Type':'application/json'
                },
                success:function (response, options) {
                    me.getStudentGridList().getStore().reload();
                },
                failure:function (response, options) {
                }
            });
        }
    },

    /**
     * Even ketika StudentGridList berubah item yang di pilih
     * @param selectionModel
     * @param selected
     */
    studentGridListSelectionChange:function (selectionModel, selected) {
        this.getStudentGridListDeleteButton().disable();
        if (!Ext.isEmpty(selected)) {
            this.getStudentGridListDeleteButton().enable();
        }
    },

    /**
     * Even klik tombol new student menampilkan StudentEditWindow dengan form kosong.
     */
    buttonAddStudentClick:function () {
        var windowEdit = Ext.create('SpringExtJS.view.student.StudentEditWindow');
        windowEdit.show();
    },

    /**
     * Even double click untu item-item dari StudentGridList.
     * Yaitu menampilkan form StudentEditWindow dengan field2 yang sudah terisi, tinggal diedit.
     * @param view
     * @param record
     */
    studentGridListItemDblClick:function (view, record) {
        var windowEdit = Ext.widget('studentEditWindow');
        windowEdit.show();
        windowEdit.down('form').loadRecord(record);
    },

    /**
     * Simpan data baru atau perubahan data ketika tombol Save pada StudentEditWindow diclick.
     */
    studentEditWindowButtonSaveClick:function () {
        var me = this;
        var form = this.getStudentEditWindowForm();
        var values = form.getValues();
        var record = form.getRecord()

        console.info("values", values.birthDate);

        if (form.getForm().isValid()) {

            //object date diubah menjadi string berformat date 'd F Y' agar bisa dikenali oleh spring Deserializer
            values.birthDate = Ext.Date.format(Ext.Date.parse(values.birthDate, 'd F Y'), 'c');
            console.info("values", values.birthDate);

            if (record) {//jika ada record berarti meng-update data lama
                Ext.Ajax.request({
                    url:studentApiUrl + "?id=" + record.data.id,
                    method:'PUT',
                    headers:{
                        'Content-Type':'application/json'
                    },
                    params:Ext.JSON.encode(values),
                    success:function (response, options) {
                        me.getStudentGridList().getStore().reload();
                    },
                    failure:function (response, options) {
                    }
                });
            } else {//tidak ada record berarti data baru
                Ext.Ajax.request({
                    url:studentApiUrl,
                    method:'POST',
                    headers:{
                        'Content-Type':'application/json'
                    },
                    params:Ext.JSON.encode(values),
                    success:function (response, options) {
                        me.getStudentGridList().getStore().reload();
                    },
                    failure:function (response, options) {
                    }
                });
            }
            this.getStudentEditWindow().close();
        }
    }
});
