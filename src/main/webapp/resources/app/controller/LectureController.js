/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/3/13
 * Time: 10:46 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('SpringExtJS.controller.LectureController', {
    extend:'Ext.app.Controller',

    views:[
        'lecture.LectureGridList',
        'lecture.LectureEditWindow'
    ],

    stores:[],

    /**
     * query selector dari component Ext JS.
     */
    refs:[
        {
            ref:'lectureGridList',
            selector:'lectureGridList'
        },
        {
            ref:'lectureGridListDeleteButton',
            selector:'lectureGridList button[action=delete]'
        },
        {
            ref:'lectureEditWindow',
            selector:'lectureEditWindow'
        },
        {
            ref:'lectureEditWindowForm',
            selector:'lectureEditWindow form'
        }
    ],

    /**
     * init..disini mendefinisikan even untuk component yang ada.
     */
    init:function () {
        this.control({
            'lectureGridList':{ //selector component, dibawahnya adalah even even yang anda definisikan
                selectionchange:this.lectureGridListSelectionChange,
                itemdblclick:this.lectureGridListItemDblClick
            },
            'lectureGridList button[action=add]':{
                click:this.buttonAddLectureClick
            },
            'lectureGridList button[action=delete]':{
                click:this.buttonDeleteLectureClick
            },
            'lectureEditWindow button[action=save]':{
                click:this.lectureEditWindowButtonSaveClick
            }
        })
    },

    /**
     * Even ketika tombol Delete Lecture ditekan. Yaitu menghapus data pada LectureGridList yang dipilih.
     */
    buttonDeleteLectureClick:function () {
        var me = this;
        Ext.Msg.show({
            title:'Are u sure?',
            msg:'Are u sure?',
            width:300,
            buttons:Ext.Msg.YESNO,
            icon:Ext.window.MessageBox.INFO,
            fn:function (buttonId) {
                if (buttonId == 'yes') {
                    me.deleteLecture();
                }
            }
        });
    },

    deleteLecture:function () {
        var me = this;
        var record = this.getLectureGridList().getSelectionModel().getSelection()[0];
        if (record) {
            Ext.Ajax.request({
                url:lectureApiUrl + "?id=" + record.data.id,
                method:'DELETE',
                headers:{
                    'Content-Type':'application/json'
                },
                success:function (response, options) {
                    me.getLectureGridList().getStore().reload();
                },
                failure:function (response, options) {
                }
            });
        }
    },

    /**
     * Even ketika LectureGridList berubah item yang di pilih
     * @param selectionModel
     * @param selected
     */
    lectureGridListSelectionChange:function (selectionModel, selected) {
        this.getLectureGridListDeleteButton().disable();
        if (!Ext.isEmpty(selected)) {
            this.getLectureGridListDeleteButton().enable();
        }
    },

    /**
     * Even klik tombol new lecture menampilkan LectureEditWindow dengan form kosong.
     */
    buttonAddLectureClick:function () {
        var windowEdit = Ext.create('SpringExtJS.view.lecture.LectureEditWindow');
        windowEdit.show();
    },

    /**
     * Even double click untu item-item dari LectureGridList.
     * Yaitu menampilkan form LectureEditWindow dengan field2 yang sudah terisi, tinggal diedit.
     * @param view
     * @param record
     */
    lectureGridListItemDblClick:function (view, record) {
        var windowEdit = Ext.widget('lectureEditWindow');
        windowEdit.show();
        windowEdit.down('form').loadRecord(record);
        windowEdit.down('combobox[name=subject]').setValue(record.data.subject.id);
        windowEdit.down('combobox[name=teacher]').setValue(record.data.teacher.id);
    },

    /**
     * Simpan data baru atau perubahan data ketika tombol Save pada LectureEditWindow diclick.
     */
    lectureEditWindowButtonSaveClick:function () {
        var me = this;
        var form = this.getLectureEditWindowForm();
        var values = form.getValues();
        var record = form.getRecord()

        if (form.getForm().isValid()) {

            values.subject = {id:values.subject};
            values.teacher = {id:values.teacher};

            if (record) {//jika ada record berarti meng-update data lama
                Ext.Ajax.request({
                    url:lectureApiUrl + "?id=" + record.data.id,
                    method:'PUT',
                    headers:{
                        'Content-Type':'application/json'
                    },
                    params:Ext.JSON.encode(values),
                    success:function (response, options) {
                        me.getLectureGridList().getStore().reload();
                    },
                    failure:function (response, options) {
                    }
                });
            } else {//tidak ada record berarti data baru
                Ext.Ajax.request({
                    url:lectureApiUrl,
                    method:'POST',
                    headers:{
                        'Content-Type':'application/json'
                    },
                    params:Ext.JSON.encode(values),
                    success:function (response, options) {
                        me.getLectureGridList().getStore().reload();
                    },
                    failure:function (response, options) {
                    }
                });
            }
            this.getLectureEditWindow().close();
        }
    }
});
