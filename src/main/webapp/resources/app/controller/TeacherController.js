/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/3/13
 * Time: 10:46 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('SpringExtJS.controller.TeacherController', {
    extend:'Ext.app.Controller',

    views:[
        'teacher.TeacherGridList',
        'teacher.TeacherEditWindow'
    ],

    stores:[],

    /**
     * query selector dari component Ext JS.
     */
    refs:[
        {
            ref:'teacherGridList',
            selector:'teacherGridList'
        },
        {
            ref:'teacherGridListDeleteButton',
            selector:'teacherGridList button[action=delete]'
        },
        {
            ref:'teacherEditWindow',
            selector:'teacherEditWindow'
        },
        {
            ref:'teacherEditWindowForm',
            selector:'teacherEditWindow form'
        }
    ],

    /**
     * init..disini mendefinisikan even untuk component yang ada.
     */
    init:function () {
        this.control({
            'teacherGridList':{ //selector component, dibawahnya adalah even even yang anda definisikan
                selectionchange:this.teacherGridListSelectionChange,
                itemdblclick:this.teacherGridListItemDblClick
            },
            'teacherGridList button[action=add]':{
                click:this.buttonAddTeacherClick
            },
            'teacherGridList button[action=delete]':{
                click:this.buttonDeleteTeacherClick
            },
            'teacherEditWindow button[action=save]':{
                click:this.teacherEditWindowButtonSaveClick
            }
        })
    },

    /**
     * Even ketika tombol Delete Teacher ditekan. Yaitu menghapus data pada TeacherGridList yang dipilih.
     */
    buttonDeleteTeacherClick:function () {
        var record = this.getTeacherGridList().getSelectionModel().getSelection()[0];
        var me = this;
        if (record) {
            Ext.Ajax.request({
                url:teacherApiUrl + "?id="  + record.data.id,
                method:'DELETE',
                headers:{
                    'Content-Type':'application/json'
                },
                success:function (response, options) {
                    me.getTeacherGridList().getStore().reload();
                },
                failure:function (response, options) {
                }
            });
        }
    },

    /**
     * Even ketika TeacherGridList berubah item yang di pilih
     * @param selectionModel
     * @param selected
     */
    teacherGridListSelectionChange:function (selectionModel, selected) {
        this.getTeacherGridListDeleteButton().disable();
        if (!Ext.isEmpty(selected)) {
            this.getTeacherGridListDeleteButton().enable();
        }
    },

    /**
     * Even klik tombol new teacher menampilkan TeacherEditWindow dengan form kosong.
     */
    buttonAddTeacherClick:function () {
        var windowEdit = Ext.create('SpringExtJS.view.teacher.TeacherEditWindow');
        windowEdit.show();
    },

    /**
     * Even double click untu item-item dari TeacherGridList.
     * Yaitu menampilkan form TeacherEditWindow dengan field2 yang sudah terisi, tinggal diedit.
     * @param view
     * @param record
     */
    teacherGridListItemDblClick:function (view, record) {
        var windowEdit = Ext.widget('teacherEditWindow');
        windowEdit.show();
        windowEdit.down('form').loadRecord(record);
    },

    /**
     * Simpan data baru atau perubahan data ketika tombol Save pada TeacherEditWindow diclick.
     */
    teacherEditWindowButtonSaveClick:function () {
        var me = this;
        var form = this.getTeacherEditWindowForm();
        var values = form.getValues();
        var record = form.getRecord()
        if (form.getForm().isValid()) {
            if (record) {//jika ada record berarti meng-update data lama
                Ext.Ajax.request({
                    url:teacherApiUrl + "?id="  + record.data.id,
                    method:'PUT',
                    headers:{
                        'Content-Type':'application/json'
                    },
                    params:Ext.JSON.encode(values),
                    success:function (response, options) {
                        me.getTeacherGridList().getStore().reload();
                    },
                    failure:function (response, options) {
                    }
                });
            } else {//tidak ada record berarti data baru
                Ext.Ajax.request({
                    url:teacherApiUrl,
                    method:'POST',
                    headers:{
                        'Content-Type':'application/json'
                    },
                    params:Ext.JSON.encode(values),
                    success:function (response, options) {
                        me.getTeacherGridList().getStore().reload();
                    },
                    failure:function (response, options) {
                    }
                });
            }
            this.getTeacherEditWindow().close();
        }
    }
});
