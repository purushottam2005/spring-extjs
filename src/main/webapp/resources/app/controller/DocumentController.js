/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/3/13
 * Time: 10:46 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('SpringExtJS.controller.DocumentController', {
    extend:'Ext.app.Controller',

    views:[
        'document.DocumentGridList',
        'document.DocumentEditWindow'
    ],

    stores:[],

    /**
     * query selector dari component Ext JS.
     */
    refs:[
        {
            ref:'documentGridList',
            selector:'documentGridList'
        },
        {
            ref:'documentGridListDeleteButton',
            selector:'documentGridList button[action=delete]'
        },
        {
            ref:'documentEditWindow',
            selector:'documentEditWindow'
        },
        {
            ref:'documentEditWindowForm',
            selector:'documentEditWindow form'
        }
    ],

    /**
     * init..disini mendefinisikan even untuk component yang ada.
     */
    init:function () {
        this.control({
            'documentGridList':{ //selector component, dibawahnya adalah even even yang anda definisikan
                selectionchange:this.documentGridListSelectionChange,
                itemdblclick:this.documentGridListItemDblClick
            },
            'documentGridList button[action=add]':{
                click:this.buttonAddDocumentClick
            },
            'documentGridList button[action=delete]':{
                click:this.buttonDeleteDocumentClick
            },
            'documentEditWindow button[action=save]':{
                click:this.documentEditWindowButtonSaveClick
            }
        })
    },

    /**
     * Even ketika tombol Delete Document ditekan. Yaitu menghapus data pada DocumentGridList yang dipilih.
     */
    buttonDeleteDocumentClick:function () {
        var record = this.getDocumentGridList().getSelectionModel().getSelection()[0];
        var me = this;
        if (record) {
            Ext.Ajax.request({
                url:documentApiUrl + "?id=" + record.data.id,
                method:'DELETE',
                headers:{
                    'Content-Type':'application/json'
                },
                success:function (response, options) {
                    me.getDocumentGridList().getStore().reload();
                },
                failure:function (response, options) {
                }
            });
        }
    },

    /**
     * Even ketika DocumentGridList berubah item yang di pilih
     * @param selectionModel
     * @param selected
     */
    documentGridListSelectionChange:function (selectionModel, selected) {
        this.getDocumentGridListDeleteButton().disable();
        if (!Ext.isEmpty(selected)) {
            this.getDocumentGridListDeleteButton().enable();
        }
    },

    /**
     * Even klik tombol new document menampilkan DocumentEditWindow dengan form kosong.
     */
    buttonAddDocumentClick:function () {
        var windowEdit = Ext.create('SpringExtJS.view.document.DocumentEditWindow');
        windowEdit.show();
    },

    /**
     * Even double click untu item-item dari DocumentGridList.
     * Yaitu mendownload file dari server.
     * @param view
     * @param record
     */
    documentGridListItemDblClick:function (view, record) {
        window.open(documentApiUrl + '/download?id=' + record.data.id);
    },

    /**
     * Simpan data baru atau perubahan data ketika tombol Save pada DocumentEditWindow diclick.
     */
    documentEditWindowButtonSaveClick:function () {
        var me = this;
        var form = this.getDocumentEditWindowForm();

        if (form.getForm().isValid()) {
            form.submit({
                url:documentApiUrl,
                waitMsg:'Uploading your document...',
                success:function (fp, o) {
                    me.getDocumentGridList().getStore().reload();
                    Ext.Msg.alert('Success', 'Upload Success');
                    me.getDocumentEditWindow().close();
                },
                failure:function (fp, o) {
                    Ext.Msg.alert('Error', 'Upload failure');
                    me.getDocumentEditWindow().close();
                }
            })
        }
    }
});
