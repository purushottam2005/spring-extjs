Ext.define('SpringExtJS.view.teacher.TeacherEditWindow', {
    extend:'Ext.window.Window',
    alias:'widget.teacherEditWindow',
    title:'Edit Teacher',
    layout:'fit',
    autoShow:true,
    resizable:false,
    modal:true,
    initComponent:function () {
        this.items = [
            {
                xtype:'form',
                padding:'5 5 0 5',
                border:false,
                style:'background-color: #fff;',
                defaults:{
                    width:400
                },
                items:[
                    {
                        xtype:'hiddenfield',
                        name:'id'
                    },
                    {
                        xtype:'textfield',
                        fieldLabel:'FirstName',
                        name:'firstName',
                        allowBlank:false
                    },
                    {
                        xtype:'textfield',
                        fieldLabel:'Last Name',
                        name:'lastName',
                        allowBlank:false
                    },
                    {
                        xtype:'combobox',
                        fieldLabel:'Academic Degree',
                        name:'academicDegree',
                        allowBlank:false,
                        displayField:'name',
                        valueField:'name',
                        store:Ext.create('Ext.data.Store', {
                            fields:['name'],
                            data:[
                                {"name":"S1"},
                                {"name":"S2"},
                                {"name":"S3"},
                                {"name":"PROF"}
                            ]
                        })
                    },
                    {
                        xtype:'combobox',
                        fieldLabel:'Sex',
                        name:'sex',
                        allowBlank:false,
                        displayField:'name',
                        valueField:'name',
                        store:Ext.create('Ext.data.Store', {
                            fields:['name'],
                            data:[
                                {"name":"MALE"},
                                {"name":"FEMALE"}
                            ]
                        })
                    },
                    {
                        xtype:'textfield',
                        fieldLabel:'Address',
                        name:'address',
                        allowBlank:false
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text:'Save',
                action:'save'
            },
            {
                text:'Cancel',
                scope:this,
                handler:this.close
            }
        ];

        this.callParent(arguments);
    }
});