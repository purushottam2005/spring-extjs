/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/3/13
 * Time: 10:49 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('SpringExtJS.view.teacher.TeacherGridList', {
    extend:'Ext.grid.Panel',
    alias:'widget.teacherGridList',
    title:'Teachers List',
    initComponent:function () {
        var store = Ext.create('SpringExtJS.store.TeacherStore');
        Ext.apply(this, {
            store:store,
            columns:[
                {header:'Id', dataIndex:'id', flex:1},
                {header:'First Name', dataIndex:'firstName', flex:1},
                {header:'Last Name', dataIndex:'lastName', flex:1},
                {header:'Academic Degree', dataIndex:'academicDegree', flex:1},
                {header:'Sex', dataIndex:'sex', flex:1},
                {header:'Address', dataIndex:'address', flex:1}
            ],
            dockedItems:[
                {
                    xtype:'toolbar',
                    dock:'top',
                    items:[
                        {
                            xtype:'button',
                            text:'New Teacher',
                            disabled:false,
                            action:'add'
                        },
                        {
                            xtype:'button',
                            text:'Delete Teacher',
                            disabled:true,
                            action:'delete'
                        }
                    ]
                },
                {
                    xtype:'pagingtoolbar',
                    dock:'bottom',
                    store:store,
                    displayInfo:true,
                    displayMsg:'Displaying Teachers {0} - {1} of {2}',
                    emptyMsg:'No Teachers to display'
                }
            ]
        });
        this.callParent(arguments);
    }
});
