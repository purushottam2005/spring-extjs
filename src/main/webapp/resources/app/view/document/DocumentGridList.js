/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/3/13
 * Time: 10:49 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('SpringExtJS.view.document.DocumentGridList', {
    extend:'Ext.grid.Panel',
    alias:'widget.documentGridList',
    title:'Documents List',
    initComponent:function () {
        var store = Ext.create('SpringExtJS.store.DocumentStore');
        Ext.apply(this, {
            store:store,
            columns:[
                {header:'Id', dataIndex:'id', flex:1},
                {header:'Original Name', dataIndex:'originalName', flex:1},
                {header:'Info', dataIndex:'info', flex:1},
                {header:'Content Type', dataIndex:'contentType', flex:1}
            ],
            dockedItems:[
                {
                    xtype:'toolbar',
                    dock:'top',
                    items:[
                        {
                            xtype:'button',
                            text:'New Document',
                            disabled:false,
                            action:'add'
                        },
                        {
                            xtype:'button',
                            text:'Delete Document',
                            disabled:true,
                            action:'delete'
                        }
                    ]
                },
                {
                    xtype:'pagingtoolbar',
                    dock:'bottom',
                    store:store,
                    displayInfo:true,
                    displayMsg:'Displaying Documents {0} - {1} of {2}',
                    emptyMsg:'No Documents to display'
                }
            ]
        });
        this.callParent(arguments);
    }
});
