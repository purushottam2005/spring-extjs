Ext.define('SpringExtJS.view.document.DocumentEditWindow', {
    extend:'Ext.window.Window',
    alias:'widget.documentEditWindow',
    title:'Upload Document',
    layout:'fit',
    autoShow:true,
    resizable:false,
    modal:true,
    initComponent:function () {
        this.items = [
            {
                xtype:'form',
                padding:'5 5 0 5',
                border:false,
                style:'background-color: #fff;',
                defaults:{
                    width:400
                },
                items:[
                    {
                        xtype:'textarea',
                        fieldLabel:'Info',
                        name:'info',
                        allowBlank:false
                    },
                    {
                        xtype:'filefield',
                        name:'file',
                        fieldLabel:'Document',
                        msgTarget:'side',
                        allowBlank:false,
                        buttonText:'Browse Document'
                    }
                ]
            }
        ];
        this.buttons = [
            {
                text:'Save',
                action:'save'
            },
            {
                text:'Cancel',
                scope:this,
                handler:this.close
            }
        ];

        this.callParent(arguments);
    }
});