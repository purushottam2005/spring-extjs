Ext.define('SpringExtJS.view.student.StudentEditWindow', {
    extend:'Ext.window.Window',
    alias:'widget.studentEditWindow',
    title:'Edit Student',
    layout:'fit',
    autoShow:true,
    resizable:false,
    modal:true,
    initComponent:function () {
        this.items = [
            {
                xtype:'form',
                padding:'5 5 0 5',
                border:false,
                style:'background-color: #fff;',
                defaults:{
                    width:400
                },
                items:[
                    {
                        xtype:'hiddenfield',
                        name:'id'
                    },
                    {
                        xtype:'textfield',
                        fieldLabel:'FirstName',
                        name:'firstName',
                        allowBlank:false
                    },
                    {
                        xtype:'textfield',
                        fieldLabel:'Last Name',
                        name:'lastName',
                        allowBlank:false
                    },
                    {
                        xtype:'combobox',
                        fieldLabel:'Sex',
                        name:'sex',
                        forceSelection:true,
                        allowBlank:false,
                        displayField:'name',
                        valueField:'name',
                        store:Ext.create('Ext.data.Store', {
                            fields:['name'],
                            data:[
                                {"name":"MALE"},
                                {"name":"FEMALE"}
                            ]
                        })
                    },
                    {
                        xtype:'datefield',
                        fieldLabel:'Birth Date',
                        name:'birthDate',
                        allowBlank:false,
                        format:'d F Y'
                    },
                    {
                        xtype:'numberfield',
                        fieldLabel:'Age',
                        name:'age',
                        minValue:0,
                        allowDecimals:false,
                        allowBlank:false
                    },
                    {
                        xtype:'textfield',
                        fieldLabel:'Address',
                        name:'address',
                        allowBlank:false
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text:'Save',
                action:'save'
            },
            {
                text:'Cancel',
                scope:this,
                handler:this.close
            }
        ];

        this.callParent(arguments);
    }
});