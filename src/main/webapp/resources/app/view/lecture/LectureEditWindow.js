Ext.define('SpringExtJS.view.lecture.LectureEditWindow', {
    extend:'Ext.window.Window',
    alias:'widget.lectureEditWindow',
    title:'Edit Lecture',
    layout:'fit',
    autoShow:true,
    resizable:false,
    modal:true,
    initComponent:function () {
        this.items = [
            {
                xtype:'form',
                padding:'5 5 0 5',
                border:false,
                style:'background-color: #fff;',
                defaults:{
                    width:400
                },
                items:[
                    {
                        xtype:'hiddenfield',
                        name:'id'
                    },
                    {
                        xtype:'combobox',
                        fieldLabel:'Subject',
                        name:'subject',
                        allowBlank:false,
                        displayField:'name',
                        valueField:'id',
                        store:Ext.create('SpringExtJS.store.SubjectStore')
                    },
                    {
                        xtype:'combobox',
                        fieldLabel:'Teacher',
                        name:'teacher',
                        allowBlank:false,
                        displayField:'firstName',
                        valueField:'id',
                        store:Ext.create('SpringExtJS.store.TeacherStore')
                    },
                    {
                        xtype:'combobox',
                        fieldLabel:'Semester',
                        name:'semester',
                        allowBlank:false,
                        displayField:'name',
                        valueField:'name',
                        store:Ext.create('Ext.data.Store', {
                            fields:['name'],
                            data:[
                                {"name":"EVEN"},
                                {"name":"ODD"}
                            ]
                        })
                    },
                    {
                        xtype:'numberfield',
                        fieldLabel:'Year',
                        name:'year',
                        minValue:1900,
                        maxValue:9999,
                        allowDecimals:false,
                        allowBlank:false
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text:'Save',
                action:'save'
            },
            {
                text:'Cancel',
                scope:this,
                handler:this.close
            }
        ];

        this.callParent(arguments);
    }
});