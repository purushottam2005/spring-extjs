/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/3/13
 * Time: 10:49 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('SpringExtJS.view.lecture.LectureGridList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.lectureGridList',
    title: 'Lectures List',
    initComponent: function () {
        var store = Ext.create('SpringExtJS.store.LectureStore');
        Ext.apply(this, {
            store: store,
            columns: [
                {header: 'Id', dataIndex: 'id', flex: 1},
                {
                    header: 'Subject',
                    dataIndex: 'subject',
                    flex: 1,
                    renderer: function (value) {
                        return value.code + " - " +value.name;
                    }
                },
                {
                    header: 'Teacher',
                    dataIndex: 'teacher',
                    flex: 1,
                    renderer: function (value) {
                        return value.firstName + " " + value.lastName;
                    }
                },
                {header: 'Semester', dataIndex: 'semester', flex: 1},
                {header: 'Year', dataIndex: 'year', flex: 1}
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        {
                            xtype: 'button',
                            text: 'New Lecture',
                            disabled: false,
                            action: 'add'
                        },
                        {
                            xtype: 'button',
                            text: 'Delete Lecture',
                            disabled: true,
                            action: 'delete'
                        }
                    ]
                },
                {
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    store: store,
                    displayInfo: true,
                    displayMsg: 'Displaying Lectures {0} - {1} of {2}',
                    emptyMsg: 'No Lectures to display'
                }
            ]
        });
        this.callParent(arguments);
    }
});
